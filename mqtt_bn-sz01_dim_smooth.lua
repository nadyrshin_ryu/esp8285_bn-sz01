station_cfg={}
station_cfg.ssid="WiFiNetworkName"
station_cfg.pwd="WiFiNetworkPassword"
station_cfg.auto=false
MQTT_BrokerIP = "192.168.1.35"
MQTT_BrokerPort = 1883
MQTT_ClientID = "esp-020"
MQTT_Client_user = "user"
MQTT_Client_password = "password"
MQTT_DimmerTopicPath = "/ESP/Dimmers/"
MQTT_Dimmer_ID = "001"
DIM_PIN = 6
LED_PIN = 7

local brightness = 1023
local target_brightness = 0
local wifi_status_old = 0

--gpio.mode(DIM_PIN, gpio.OUTPUT) 
gpio.mode(LED_PIN, gpio.OUTPUT)
gpio.write(LED_PIN, gpio.LOW)
pwm.setup(DIM_PIN, 100, brightness)
pwm.start(DIM_PIN)

wifi.setmode(wifi.STATION)
wifi.sta.config(station_cfg)
wifi.sta.connect()

local function setDimmer(id, state)
    local value = tonumber(state)
    if value ~= nil then
        --print(value)
        if value > 100 then
            value = 100
        end
        target_brightness = math.floor(value * 10.23) -- приводим % в величину для ШИМ (10 бит)
        --print(target_brightness)
    end
end

-- Таймер плавного изменения яркости
tmr.alarm(1, 20, tmr.ALARM_AUTO, function()
	if target_brightness ~= brightness then
		local delta = target_brightness - brightness
		local adelta = math.abs(delta)
		local step = 0
		
		if adelta > 10 then	-- шагать будем быстро
			step = 10
		else				-- шагать будем медленно
			step = 1
		end
		
		if (delta < 0) then
			step = -step
		end
		
		brightness = brightness + step
		--print(brightness)
       	pwm.setduty(DIM_PIN, brightness)
	end
end)

tmr.alarm(0, 5000, tmr.ALARM_AUTO, function()
    --print("tmr0 "..wifi_status_old.." "..wifi.sta.status())

    if wifi.sta.status() == 5 then -- подключение есть
        if wifi_status_old ~= 5 then -- Произошло подключение к Wifi, IP получен
            print(wifi.sta.getip())

            m = mqtt.Client(MQTT_ClientID, 120, MQTT_Client_user, MQTT_Client_password)

            -- Определяем обработчики событий от клиента MQTT
            m:on("connect", function(client) print ("connected") end)
            m:on("offline", function(client) 
                tmr.stop(1)
				gpio.write(LED_PIN, gpio.LOW)
				print ("offline") 
            end)
            m:on("message", function(client, topic, data) 
                --print(topic .. ":" ) 
                if data ~= nil then
                    --print(data)
                end

                local _, Pos = string.find(topic, MQTT_DimmerTopicPath.."(%w)")
                local Dimmer = string.sub(topic, Pos)
                --print(Dimmer)
                if data ~= nil then
                    setDimmer(Dimmer, data)
                end
            end)

            m:connect(MQTT_BrokerIP, MQTT_BrokerPort, 0, 1, function(conn) 
                print("connected")
				gpio.write(LED_PIN, gpio.HIGH)
                
                -- Подписываемся на топики если нужно
                m:subscribe(MQTT_DimmerTopicPath.."#",0, function(conn) 
                    print("Subscribed!")
                end)
            end)
        else
            -- подключение есть и не разрывалось, ничего не делаем
        end
    else
        print("Reconnect "..wifi_status_old.." "..wifi.sta.status())
        tmr.stop(1)
        wifi.sta.connect()
    end

    -- Запоминаем состояние подключения к Wifi для следующего такта таймера
    wifi_status_old = wifi.sta.status()
end)
